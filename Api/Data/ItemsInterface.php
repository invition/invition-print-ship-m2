<?php
namespace Invition\InvitionPrintShipM2\Api\Data;


interface ItemsInterface
{
	const ITEM_ID 		= 'item_id';
	const PRODUCTNAME	= 'productname';
	
	public function getId();
	public function getProductname();
	public function setId($id);
	public function setProductname($productname);
	
}
