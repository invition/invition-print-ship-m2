<?php 

namespace Invition\InvitionPrintShipM2\Model;

use Invition\InvitionPrintShipM2\Api\Data\ItemsInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Items  extends \Magento\Framework\Model\AbstractModel implements ItemsInterface, IdentityInterface
{
	/**
     * CMS page cache tag
     */
    const CACHE_TAG = 'invitionprintshipm2_items';

    /**
     * @var string
     */
    protected $_cacheTag = 'invitionprintshipm2_items';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'invitionprintshipm2_items';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Invition\InvitionPrintShipM2\Model\ResourceModel\Items');
    }
    
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ITEM_ID);
    }    

    /**
     * Get Productname
     *
     * @return string|null
     */
    public function getProductname()
    {
        return $this->getData(self::PRODUCTNAME);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Invition\InvitionPrintShipM2\Api\Data\ItemInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ITEM_ID, $id);
    }

    /**
     * Set Productname
     *
     * @param string $productname
     * @return \Invition\InvitionPrintShipM2\Api\Data\ItemInterface
     */
    public function setProductname($productname)
    {
        return $this->setData(self::PRODUCTNAME, $productname);
    }
    
}
