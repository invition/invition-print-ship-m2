<?php
namespace Invition\InvitionPrintShipM2\Model\Config\Backend\Serialized;

/**
 * Backend system config array field renderer
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class ArraySerialized extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
	public function addColumn($name, $params)
    {
		
		  $this->_columns[$name] = array(
            'label' => empty($params['label']) ? 'Column' : $params['label'],
            'size' => empty($params['size']) ? false : $params['size'],
            'style' => empty($params['style']) ? null : $params['style'],
            'class' => empty($params['class']) ? null : $params['class'],
            'renderer' => false,
            'type' => 'text',
            'options' => false,
        );
        if ((!empty($params['renderer'])) && ($params['renderer'] instanceof Mage_Core_Block_Abstract)) {
            $this->_columns[$name]['renderer'] = $params['renderer'];
        }

        if ((!empty($params['type']))) {
            $this->_columns[$name]['type'] = $params['type'];
        }

        if ((!empty($params['options']))) {
            $this->_columns[$name]['options'] = $params['options'];
        }
    }
	
	public function renderCellTemplate($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new \Exception('Wrong column name specified.');
        }
        $column = $this->_columns[$columnName];
        $inputName = $this->_getCellInputElementName($columnName);

        if ($column['renderer']) {
            return $column['renderer']->setInputName(
                $inputName
            )->setInputId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setColumnName(
                $columnName
            )->setColumn(
                $column
            )->toHtml();
        }

	    switch ($column['type']) {
            case "newrow":
                $html = "</tr><tr>";
                break;
            case "hidden":
                $html = '<input type="hidden" name="' . $inputName . '" value="#{' . $columnName . '}" ' . ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' . (isset($column['class']) ? $column['class'] : 'input-text') . '"' . (isset($column['style']) ? ' style="' . $column['style'] . '"' : '') . '/>';
                break;
            case "readonly":
                $html = '<input type="text" readonly name="' . $inputName . '" value="#{' . $columnName . '}" ' . ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' . (isset($column['class']) ? $column['class'] : 'input-text') . '"' . (isset($column['style']) ? ' style="' . $column['style'] . '"' : '') . '/>';
                break;
            case "text":
                $html = '<input type="text" id="' . $this->_getCellInputElementId(
            '<%- _id %>',
            $columnName
        ) .
            '"' .
            ' name="' .
            $inputName .
            '" value="<%- ' .
            $columnName .
            ' %>" ' .
            ($column['size'] ? 'size="' .
            $column['size'] .
            '"' : '') .
            ' class="' .
            (isset(
            $column['class']
        ) ? $column['class'] : 'input-text') . '"' . (isset(
            $column['style']
        ) ? ' style="' . $column['style'] . '"' : '') . '/>';
                break;
			 case "textarea":
                $html = '<textarea type="text" name="' . $inputName . '" ' . ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' . (isset($column['class']) ? $column['class'] : 'input-text') . '"' . (isset($column['style']) ? ' style="' . $column['style'] . '"' : '') . '/>#{' . $columnName . '}</textarea>';
                break;
            case "file":
                $html = '<input type="file" name="' . $inputName . '"' . ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' . (isset($column['class']) ? $column['class'] : '') . '"' . (isset($column['style']) ? ' style="' . $column['style'] . '"' : '') . '/>';
                $html .= '#{' . $columnName . '}';
                break;
            case "multiselect":
                $html = '<input type="hidden" value="#{' . $columnName . '}" disabled="disabled" class="is-enabled-hidden" />';
                $html .= '<select name="' . $inputName . '[]" value="#{' . $columnName . '}" ' . ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' . (isset($column['class']) ? $column['class'] : 'select multiselect') . '"' . (isset($column['style']) ? ' style="' . $column['style'] . '"' : '') . ' multiple=multiple>';

                if (is_array($column['options'])) {
                    foreach ($column['options'] as $key => $val) {
                        $html .= '<option value="' . $key . '">' . $val . '</option>';
                    }
                }

                $html .= "</select>";
                break;
            case "select":
                $html = '<input type="hidden" value="#{' . $columnName . '}" disabled="disabled" class="is-enabled-hidden" />';
                $html .= '<select name="' . $inputName . '" value="#{' . $columnName . '}" ' . ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' . (isset($column['class']) ? $column['class'] : 'select') . '"' . (isset($column['style']) ? ' style="' . $column['style'] . '"' : '') . '>';

                if (is_array($column['options'])) {
                    foreach ($column['options'] as $key => $val) {
						
                        $html .= '<option value="' . $key . '">' . $val . '</option>';
                    }
                }

                $html .= "</select>";
                break;
        }
		 
		 
		return $html;
		 
        
    }
}
?>