<?php

namespace Invition\InvitionPrintShipM2\Model\System\Config;

/**
 * Price types mode source
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Attributes extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * @var \Magento\Eav\Model\EntityFactory
     */
    protected $eavEntityFactory;

    public function __construct(
        \Magento\Eav\Model\EntityFactory $eavEntityFactory
    ) {
        $this->eavEntityFactory = $eavEntityFactory;
    }
    public function getAllOptions()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$entityTypeId = $this->eavEntityFactory->create()->setType('catalog_product')->getTypeId();		 
		 
		$coll = $objectManager->create('\Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection');
		$coll->addFieldToFilter(\Magento\Eav\Model\Entity\Attribute\Set::KEY_ENTITY_TYPE_ID, $entityTypeId);
		$coll = $coll->load();

		$result = array();
		
		foreach($coll as $attributeset) {
			$item = array();
			$item["value"] = $attributeset->getName();
			$item["label"] = $attributeset->getName();			

			array_push($result, $item);		
		}
		
		   
		return $result;
    }
 
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
	
    
}
