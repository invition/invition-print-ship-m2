<?php

namespace Invition\InvitionPrintShipM2\Model\System\Config;

/**
 * Price types mode source
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Apiversions extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * @var \Magento\Eav\Model\EntityFactory
     */
    protected $eavEntityFactory;

    public function __construct(
        \Magento\Eav\Model\EntityFactory $eavEntityFactory
    ) {
        $this->eavEntityFactory = $eavEntityFactory;
    }
    public function getAllOptions()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$result = array();
		
		array_push($result, $this->addItem('prod'));
		array_push($result, $this->addItem('test'));
		
		return $result;
    }
 
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
	
 	private function addItem($label) {
		$item = array();

		$item["value"] = $label;
		$item["label"] = $label;			

		return $item;
	}
    
}
