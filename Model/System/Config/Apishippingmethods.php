<?php

namespace Invition\InvitionPrintShipM2\Model\System\Config;

/**
 * Price types mode source
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Apishippingmethods extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * @var \Magento\Eav\Model\EntityFactory
     */
    protected $eavEntityFactory;

    public function __construct(
        \Magento\Eav\Model\EntityFactory $eavEntityFactory
    ) {
        $this->eavEntityFactory = $eavEntityFactory;
    }
	
	 public function toOptionHash()
    {
		$shippingmethods = $this->getShippingMethods();

        $data = array();
        foreach ($shippingmethods as $item) {
			
			$methodname = $item->name;
			$id = $item->id;
			 
            $data[strval($id)] = $methodname;
        }
 
        return $data;
    }
	
    public function getAllOptions()
    {

		$shippingmethods = $this->getShippingMethods();
      
		$result = array();
		
		foreach($shippingmethods as $item) {
			$methodname = $item->name;
			$id = $item->id;
			
			$methodname = str_replace("'", "", $methodname);
			$methodname = str_replace("\r", "", $methodname);
			$methodname = str_replace("\n", "", $methodname);
			
			array_push($result, $this->addItem($id, $methodname));
		
		}
		 	
		return $result;
    }
 
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
	
 	private function addItem($id, $label) {
		$item = array();

		$item["value"] = $id;
		$item["label"] = $label;			

		return $item;
	}
	
	
	public function getShippingMethods() {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$clientcon = $objectManager->create('Invition\InvitionPrintShipM2\Model\Clientcon');
		$shippingmethods = $clientcon->getShippingMethods();

		if ($shippingmethods) {
			return $shippingmethods->methods;	
		}

		return array();		
		
	}
    
}
