<?php

namespace Invition\InvitionPrintShipM2\Model\System\Config;

/**
 * Price types mode source
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Categories extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * @var \Magento\Eav\Model\EntityFactory
     */
    protected $eavEntityFactory;
	private $categoryCollectionFactory;
	private $categoryHelper;
	
    public function __construct(
        \Magento\Eav\Model\EntityFactory $eavEntityFactory		,        
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Helper\Category $categoryHelper
    ) {
        $this->eavEntityFactory = $eavEntityFactory;
		$this->categoryCollectionFactory = $categoryCollectionFactory;
		$this->categoryHelper = $categoryHelper;
    }
    public function getAllOptions()
    {
		$categories = $this->getCategoryCollection();
		
		$result = array();
		foreach ($categories as $categorie) {
			$item = array();
			$item["value"] = $categorie->getId();
			$item["label"] = $categorie->getName();			

			array_push($result, $item);
		}
		return $result;
    }
	
	 public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');        
        
        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
                
        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }
        
        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
        
        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize); 
        }    
        
        return $collection;
    }
	
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
	
 	private function addItem($label) {
		$item = array();

		$item["value"] = $label;
		$item["label"] = $label;			

		return $item;
	}
    
}
