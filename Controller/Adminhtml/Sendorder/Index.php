<?php namespace Invition\InvitionPrintShipM2\Controller\Adminhtml\Sendorder;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory

    ) {
		
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
 
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Invition_InvitionPrintShipM2::items');
        $resultPage->addBreadcrumb(__('InvitionPrintShipM2'), __('InvitionPrintShipM2'));
        $resultPage->addBreadcrumb(__('Send order'), __('Send order'));
        $resultPage->getConfig()->getTitle()->prepend(__('Send order'));

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$data = $this->getRequest()->getParams();
 
		$orderId = $data["id"];
	 
		$orderRepository = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface');
		
		$order = $orderRepository->get($orderId);;
 		
		$model = $objectManager->create('Invition\InvitionPrintShipM2\Model\Orderupdater');
		$model->submitOrder($order);
		
		
		$resultRedirect = $this->resultRedirectFactory->create();				
	  
		
		$urlBuilder = $objectManager->create('\Magento\Framework\UrlInterface');
		
		$url = $urlBuilder->getUrl('sales/order/view/', array("order_id"=> $orderId));

		return  $resultRedirect->setPath($url);
	
	}

    /**
     * Is the user allowed to view the blog post grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Invition_InvitionPrintShipM2::items');
    }

}
