<?php
namespace Invition\InvitionPrintShipM2\Cron;

class Updateproductdata {
 
    protected $_logger;
 	   	
    public function __construct(\Psr\Log\LoggerInterface $logger) {
        $this->_logger = $logger;
	}
 
    public function execute() {
        $this->_logger->info(__METHOD__);
		echo date("Y-m-d h:m:s", time()) . " Cronjob started - Update productdata \r\n";
					 		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		 
		$model = $objectManager->create('Invition\InvitionPrintShipM2\Model\Productupdater');
		$model->updateProducts();
			
        return $this;
    }
	
}
