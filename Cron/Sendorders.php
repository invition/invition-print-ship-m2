<?php
namespace Invition\InvitionPrintShipM2\Cron;

class Sendorders {
 
    protected $_logger;
	
    public function __construct(\Psr\Log\LoggerInterface $logger) {
        $this->_logger = $logger;
	}
 
    public function execute() {
        $this->_logger->info(__METHOD__);
		echo date("Y-m-d h:m:s", time()) . " Cronjob started - Commit orders \r\n";
		$target_dir = BP . "/";
		
		$lockfile = $target_dir . ".locksendorders_run";
		
		if (!file_exists($lockfile)) {

			fopen($lockfile, "w");			 		

			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

			$model = $objectManager->create('Invition\InvitionPrintShipM2\Model\Orderupdater');
			$model->checkForOrdersToSubmit();
			
			unlink($lockfile);	
		}
		
        return $this;
    }
	
}
